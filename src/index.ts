import { FormControl } from "./tsModule/ts/formControl";
import { FormGroup } from "./tsModule/ts/formGroup";
import { RequireValidator } from "./tsModule/ts/validator/requaerValidator";
import { EmailValidator } from "./tsModule/ts/validator/emailValidator";
import { PatternValidator } from "./tsModule/ts/validator/patternValidator";
import { MaxValueValidator } from "./tsModule/ts/validator/maxValueValidator";
import { MinValueValidator } from "./tsModule/ts/validator/minValueValidator";
import { NumberValidator } from "./tsModule/ts/validator/numberValidator";
import { MinLengthValidator } from "./tsModule/ts/validator/minLengthValidator";
import { MaxLengthValidator } from "./tsModule/ts/validator/maxLengthValidator";

const formGroup = new FormGroup('test-form', [
  new FormControl('input-1', null, [new RequireValidator()]),
  new FormControl('input-2', null, [new EmailValidator()]),
  new FormControl('input-3', null, [new NumberValidator()]),
  new FormControl('input-4', null, [new MinLengthValidator(4)]),
  new FormControl('input-5', null, [new MaxLengthValidator(10)]),
  new FormControl('input-6', null, [new MaxValueValidator(5)]),
  new FormControl('input-7', null, [new MinValueValidator(10)]),
  new FormControl('input-8', null, [new PatternValidator(/[0-9]{4} *[0-9]{4} *[0-9]{4} *[0-9]{4}/)])

]);

// console.log(formGroup.value);

formGroup.valueChange.subscribe((change: any) => {
  console.log('FormGroup change', change);
});
