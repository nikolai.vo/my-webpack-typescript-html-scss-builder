import { Validator } from './validator';

//валидация на минимальное число
export class MinValueValidator extends Validator {
  constructor(private minValue: number) {
    super();

    this.minValue = minValue;

  }
  validate(value: number): boolean {
    return value > this.minValue
  }
}
