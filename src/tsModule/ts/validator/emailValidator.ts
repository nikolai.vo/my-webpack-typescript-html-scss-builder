import { Validator } from './validator';
//валидация емейла
export class EmailValidator extends Validator {

  private regExp: RegExp;
  constructor() {
    super();
    this.regExp = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;// проверка на емейл
  }

  validate(value: string) {
    return this.regExp.test(value);// test ищет совпадения в  регулярке и value
  }
}
