import { Validator } from './validator';

//валидация на максимальное число
export class MaxValueValidator extends Validator {

  constructor(private maxValue: number) {
    super();

    this.maxValue = maxValue;

  }
  validate(value: number): boolean {
    return value < this.maxValue;
  }
}
