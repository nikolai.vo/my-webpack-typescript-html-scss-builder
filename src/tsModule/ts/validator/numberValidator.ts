import { Validator } from './validator';


//валидация на число
export class NumberValidator extends Validator {

  private regExp: RegExp;
  constructor() {
    super();
    this.regExp = /\-?\d+(\.\d{0,})?/; //целые числа и числа с плавающей точкой
  }

  validate(value: string): boolean {
    return this.regExp.test(value);
  }
}
