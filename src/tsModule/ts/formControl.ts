import { EventEmitter } from './eventEmitter';
import { Validator } from './validator/validator';
import { FormControlModel } from './InterfaseFormControlModel';

export class FormControl implements FormControlModel{
  public id: string
  private input: HTMLInputElement;
  public status: boolean;
  public valueChange: EventEmitter<{ inputIdValue: string, statusValue: boolean }>;
  public statusChange: EventEmitter<{ inputId: string, value: boolean }>;
  public value: string;

  constructor(id: string, private initialValue: string | number | null, private validators: Array<Validator>) {
    this.id = id;
    this.initialValue = initialValue;
    this.validators = validators;
    this.status = false;
    this.valueChange = new EventEmitter();
    this.statusChange = new EventEmitter();
    this.input = document.getElementById(id) as HTMLInputElement;
    this.value = this.input.value;

    // регистрируем обработчик события change для формы
    this.input.addEventListener('change', (e: Event) => {

      //every возвращает true, если вызов callback вернёт true для каждого элемента
      this.status = this.validators.every(function (valid: Validator) {
        return valid.validate((e.target as HTMLInputElement).value);
      });

      this.value = this.input.value;

      //генерируем события
      this.valueChange.emit({
        inputIdValue: this.value,
        statusValue: this.status

      });

      

      if (this.status === false) {
        this.input.classList.add('my-input--red');
      } else {
        this.input.classList.remove('my-input--red');
      }
    });
  }
}
